#!/usr/bin/env python
# coding: utf-8

# In[38]:


import arrow
import pymongo

client = pymongo.MongoClient('localhost')
db = client['buttonresponse']

# Fetch our series collection
series_collection = db['br']
mes_col = db['mes']


def pushbutton(username, imgattach, lentime):
    #Perform a button push based on current time. Give it 
    #database name, username, and amount button is held for. 
    #conn = sqlite3.connect('{}.db'.format(namedatabase))

    datetime = arrow.now()
    duedate = datetime.shift(hours=lentime)
    
    post = {'image' : 'https://printrecsup.s3.amazonaws.com/' + imgattach,
            'username' : username, 'length' : lentime,
           'datetime': datetime.timestamp, 'datedue' : duedate.timestamp}
    return(series_collection.insert_one(post).inserted_id)



def createtext(friendname, username, message, imgattach):
    #creates a database entry from friend to username. Input friendname, their 
    #username, and the message. This is then saved in the database for when 
    #username pushes the button. 
    
    datetime = arrow.now()
     post = {'image' : 'https://printrecsup.s3.amazonaws.com/' + imgattach,
            'username' : username, 'friendname' : friendname,
           'message' : message, 'datetime': datetime.timestamp}
    return(mes_col.insert_one(post).inserted_id)


# In[ ]:





# In[40]:


#pushbutton('will', 'hello.png', 11)


# In[53]:


def find_document(elements, multiple=True):

    return series_collection.find(elements)


# In[57]:


def find_msg(elements, multiple=True):

    fot =  mes_col.find(elements)
    for fo in fot:
        print(fo)


# In[ ]:





# In[54]:


#find_document({'length' : 11})


# In[58]:


#find_msg({'username': 'will'})


# In[ ]:





# In[ ]:





# In[51]:


#createtext('joe', 'will', 'now now', 'hello.png')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




